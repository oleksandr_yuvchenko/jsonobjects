import codecs
import os
import re
import sys
from os.path import exists, join

from setuptools import setup

name = "jsonobjects"
package = "jsonobjects"
description = "JSONObjects allows you to declaratively specify how to extract and convert elements from a JSON document."
author = "Splitmetrics"

install_requires = [
    "dateutils == 0.6.12",
    "jmespath == 0.10.0",
    "mock == 4.0.3",
]


def get_version(package):
    """
    Return package version as listed in `__version__` in `init.py`.
    """
    init_py = open(join(package, "__init__.py")).read()
    version = re.search("^__version__ = ['\"]([^'\"]+)['\"]", init_py, re.MULTILINE).group(1)
    if not version:
        raise RuntimeError("Cannot find version information")
    return version


def get_readme(package):
    with codecs.open("README.rst", "r", "utf-8") as fd:
        return fd.read()


def get_packages(package):
    """
    Return root package and all sub-packages.
    """
    return [
        dirpath
        for dirpath, dirnames, filenames in os.walk(package)
        if os.path.exists(os.path.join(dirpath, "__init__.py"))
    ]


def get_package_data(package):
    """
    Return all files under the root package, that are not in a
    package themselves.
    """
    walk = [
        (dirpath.replace(package + os.sep, "", 1), filenames)
        for dirpath, dirnames, filenames in os.walk(package)
        if not exists(join(dirpath, "__init__.py"))
    ]

    filepaths = []
    for base, filenames in walk:
        filepaths.extend([join(base, filename) for filename in filenames])
    return {package: filepaths}


setup(
    name=name,
    version=get_version(package),
    description=description,
    long_description=get_readme(package),
    author=author,
    packages=get_packages(package),
    package_data=get_package_data(package),
    install_requires=install_requires,
)
